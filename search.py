#!/usr/bin/env python2
# -*- coding:utf-8 -*-

import argparse
import betfair

eventURL = "https://betfair.es/exchange/%s/event?id=%s" 

#Config de argparse
description = "Buscador de eventos a partir de los par�metros pasados"

parser = argparse.ArgumentParser(description=description)
parser.add_argument("-m", help="Tipo(s) de mercado(s) a buscar", nargs="+", required=True)
parser.add_argument("-e", help="Índice del elemento(s) de cada mercado a los que buscar", nargs="*", type=int, required=True)
parser.add_argument("-l", help="Cuota m�nima del lay en cada mercado", nargs="*", type=float)
parser.add_argument("-L", help="Cuota m�xima del lay en cada mercado", nargs="*")
parser.add_argument("-b", help="Cuota m�nima del back en cada mercado", nargs="*", type=float)
parser.add_argument("-B", help="Couta m�xima del back en cada mercado", nargs="*")
parser.add_argument("-a", help="Cantidad m�nima disponible para apostar en cada mercado", nargs="*")
parser.add_argument("-A", help="Cantidad m�xima disponible para apostar en cada mercado", nargs="*")
parser.add_argument("-i", help="Cantidad m�nima igualada en cada mercado", nargs="*")
parser.add_argument("-I", help="Cantidad m�xima igualada en cada mercado", nargs="*")
parser.add_argument("-s", help="Deporte, por defecto football", type=str, default="football")

args = parser.parse_args()
numMercados = len(args.m)
mercados = args.m
elementIndex = args.e

manager = betfair.Manager(args.s)

for event in manager.events():
	check = True

	for market in event.markets():
		count = 0

		if market.type() in mercados:
			element = market.elements()[elementIndex[count]]

			for argumento in vars(args):
				argsDict = vars(args)
				isNone = lambda x: argsDict[x] is None

				if not isNone(argumento):
					#A falta de switch case... dicts :)
					try:
		 				check = check and \
	 					{
							'l': (False if args.l[count] >= element.layOdds()[0] else True) if not isNone('l') else 0,
							'L': (False if args.L[count] <= element.layOdds()[0] else True) if not isNone('L') else 0,
							'b': (False if args.b[count] >= element.backOdds()[0] else True) if not isNone('b') else 0,
							'B': (False if args.B[count] <= element.backOdds()[0] else True) if not isNone('B') else 0,
							'a': (False if args.a[count] >= market.totalAvailable() else True) if not isNone('a') else 0,
							'A': (False if args.A[count] <= market.totalAvailable() else True) if not isNone('A') else 0,
							'i': (False if args.i[count] >= market.totalMatched() else True) if not isNone('i') else 0,
							'I': (False if args.I[count] <= market.totalMatched() else True) if not isNone('I') else 0,
							'e': True,
							'm': True,
							's': True
						}[argumento]
					except Exception as e:
						#Seguramente no hay dinero en el mercado
						print "[ERROR]:", e,  event.name()
						check = False
			count += 1

	if check:
		print event.name() + ":"
		print "\t", eventURL % (args.s, event.id())
