#!/usr/bin/env python2
# -*- coding:utf-8 -*_

import sys
import betfair as bf

if len(sys.argv) == 1:
	print "Usage:"
	print "\t./volume.py <event_url1> <event_url2> ... <event_urlN> <market_type>\n\n"
	print "Script que devuelve la cantidad de dinero disponible en apuestas, y la"
	print "cantidad de dinero igualado en un mercado de diferentes eventos.\n"
	print "Tipos de mercado disponibles:"
	print "\tMATCH_ODDS:    Cuotas del partido"
	print "\tCORRECT_SCORE: Marcador correcto"
	print "\tOVER_UNDER_XX: M�s menos XX goles, cambiando XX por"
	print "\t               05 (0.5), 15 (1.5), 25 (2.5)...\n"
	print "NOTA:"
	print "\tPuede que algun mercado no est� disponible en determinados eventos."

numEventos = len(sys.argv)-2

for i in range(numEventos):
	event = bf.Event(sys.argv[i+1])
	print event.name() + ":"
	for market in event.markets():
		if market.type() == sys.argv[-1]:
			print "\tDisponibles:", str(market.totalAvailable()) + "€"
			print "\tIgualados:", str(market.totalMatched()) + "€"
